var MowjowCrowdsale = artifacts.require("./MowjowCrowdsale.sol");

module.exports = function (deployer, network, accounts) {
    const gasValue = { gas: 100000000 };

    const startTime = web3.eth.getBlock(web3.eth.blockNumber).timestamp + 1 // one second in the future
    const endTime = startTime + (86400 * 20) // 20 days 
    const rate = new web3.BigNumber(100); 
    const cap = new web3.BigNumber(100); 
    const multisigWallet = accounts[3];

    deployer.deploy(MowjowCrowdsale, startTime, endTime, rate, multisigWallet, cap, gasValue);
};