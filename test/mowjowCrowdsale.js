const ether = require('./helpers/ether')
const advanceBlock = require('./helpers/advanceToBlock')
const increaseTimeTo = require('./helpers/increaseTime').increaseTimeTo
const duration = require('./helpers/increaseTime').duration
const latestTime = require('./helpers/latestTime')
const EVMThrow = require('./helpers/EVMThrow')

const BigNumber = web3.BigNumber

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should()

const MowjowCrowdsale = artifacts.require('MowjowCrowdsale')
const MowjowToken = artifacts.require('MowjowToken')

contract('MowjowCrowdsale', function ([_, investor, wallet, purchaser]) {
  const cap = ether(3)
  const lessThanCap = ether(1)
  const rate = new BigNumber(20000)
  const value = ether(20)

  const expectedTokenAmount = rate.mul(value)

  before(async function () {
    //Advance to the next block to correctly read time in the solidity "now" function interpreted by testrpc
    await advanceBlock()
  })

  beforeEach(async function () {
    this.startTime = latestTime() + duration.weeks(1);
    this.endTime = this.startTime + duration.weeks(1);
    this.afterEndTime = this.endTime + duration.seconds(1)

    this.mowjowCrowdsale = await MowjowCrowdsale.new(this.startTime, this.endTime, rate, wallet, cap)
    let tokenAddress = await this.mowjowCrowdsale.token();
    this.token = MowjowToken.at(await this.mowjowCrowdsale.token())
  })

  describe('creating a valid crowdsale', function () {

    it('should fail with zero cap', async function () {
      await MowjowCrowdsale.new(this.startTime, this.endTime, rate, wallet, 0).should.be.rejectedWith(EVMThrow);
    })

  });

  describe('accepting payments', function () {

    beforeEach(async function () {
      await increaseTimeTo(this.startTime)
    })

    it('should be token owner', async function () {
      const owner = await this.token.owner()
      owner.should.equal(this.mowjowCrowdsale.address)
    })

    it('should be ended only after end', async function () {
      let ended = await this.mowjowCrowdsale.hasEnded()
      ended.should.equal(false)
      await increaseTimeTo(this.afterEndTime)
      ended = await this.mowjowCrowdsale.hasEnded()
      ended.should.equal(true)
    })

    it('should accept payments within cap', async function () {
      await this.mowjowCrowdsale.send(cap.minus(lessThanCap)).should.be.fulfilled
      await this.mowjowCrowdsale.send(lessThanCap).should.be.fulfilled
    })

    it('should reject payments outside cap', async function () {
      await this.mowjowCrowdsale.send(cap)
      await this.mowjowCrowdsale.send(1).should.be.rejectedWith(EVMThrow)
    })

    it('should reject payments that exceed cap', async function () {
      await this.mowjowCrowdsale.send(cap.plus(1)).should.be.rejectedWith(EVMThrow)
    })

  })

  describe('ending', function () {

    beforeEach(async function () {
      await increaseTimeTo(this.startTime)
    })

    it('should not be ended if under cap', async function () {
      let hasEnded = await this.mowjowCrowdsale.hasEnded()
      hasEnded.should.equal(false)
      await this.mowjowCrowdsale.send(lessThanCap)
      hasEnded = await this.mowjowCrowdsale.hasEnded()
      hasEnded.should.equal(false)
    })

    it('should not be ended if just under cap', async function () {
      await this.mowjowCrowdsale.send(cap.minus(1))
      let hasEnded = await this.mowjowCrowdsale.hasEnded()
      hasEnded.should.equal(false)
    })

    it('should be ended if cap reached', async function () {
      await this.mowjowCrowdsale.send(cap)
      let hasEnded = await this.mowjowCrowdsale.hasEnded()
      hasEnded.should.equal(true)
    })
  })
  
})
