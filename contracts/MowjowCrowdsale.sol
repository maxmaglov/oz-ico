pragma solidity ^0.4.11; 

import 'zeppelin-solidity/contracts/token/MintableToken.sol';
import 'zeppelin-solidity/contracts/crowdsale/CappedCrowdsale.sol';
import './MowjowToken.sol';

contract MowjowCrowdsale is CappedCrowdsale { 

    // The token being sold
    MowjowToken public token;

    function MowjowCrowdsale(uint256 _startTime, uint256 _endTime, uint256 _rate, address _wallet, uint256  cap )
    CappedCrowdsale(cap) Crowdsale(_startTime, _endTime,  _rate, _wallet) {
    }

    // creates the token to be sold.
    // override this method to have crowdsale of a specific MowjowToken token.
    function createTokenContract() internal returns (MintableToken) {
        token = new MowjowToken();
        return token;
    }

}
