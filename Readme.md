## Install

> npm install truffle -g

## Quick Start Guide 

> npm install  

Start testnet

> npm run rpc 
 
Should open new terminal window

> truffle migrate

> truffle test 